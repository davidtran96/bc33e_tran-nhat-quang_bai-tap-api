
let dssv = [];
let BASE_URL = "https://62f8b755e0564480352bf404.mockapi.io";

var tatLoading = function (){
  document.getElementById("loading").style.display = "none";
}
var batLoading = function (){
 document.getElementById("loading").style.display="flex";
}
tatLoading();

let renderTable = function (list) {
  var contentHtml = "";
  list.forEach(function (item) {
    var trContent = `
    <tr>
      <td>${item.ma}</td>
      <td>${item.ten}</td>
      <td>${item.email}</td>
      <td><img src=${item.hinhAnh} style="width:40px" alt="" />
      </td>
      <td>
      <button onclick="xoaSinhVien('${item.ma}')"
      class ="btn btn-danger">Xoá</button>
      <button onclick="suaSinhVien('${item.ma}')"
      class ="btn btn-warning">Sửa</button>
      </td>
    </tr>`;
    contentHtml += trContent;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHtml;
};

var renderDssvService = function(){
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (response) {
      dssv = response.data;
      console.log("dssv: ", dssv);
      renderTable(dssv);
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
  
}
renderDssvService();

function xoaSinhVien(id) {
  console.log("id: ", id);
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
  .then(function () {
    renderDssvService();
    tatLoading();
  })
  .catch(function (err) {
    tatLoading();
    console.log(err);
  });
}

function themSV(){
  var dataForm = layThongTinTuForm();
  console.log('data: ', dataForm);
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: dataForm,
  })
  .then(function () {
    renderDssvService();
    tatLoading();
  })
  .catch(function (err) {
    tatLoading();
    console.log(err);
  });
}

function suaSinhVien(id){
  batLoading();

  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
  .then(function (response) {
   tatLoading(); 
   console.log(response);
   showThongTinLenForm(response.data);
  })
  .catch(function (err) {
    tatLoading();
    console.log(err);
  })
}
function capNhatSV(){
  var dataForm = layThongTinTuForm();
  console.log('dataForm: ', dataForm);
  var id = dataForm.ma;
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "PUT",
    data: dataForm,
  })
  .then(function (response) {
    tatLoading();
    console.log(response);
    renderDssvService();
   })
   .catch(function (err) {
    tatLoading();
    console.log(err);
   })

}